from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated

from apps.order.api.filter import OrderFilter
from apps.order.api.paginations import UserOrderPagination
from apps.order.api.serializers import OrderSerializer
from apps.order.models import Order


class OrderViewSet(mixins.ListModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.RetrieveModelMixin,
                   viewsets.GenericViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filterset_class = OrderFilter
    pagination_class = UserOrderPagination
    permission_classes = [IsAuthenticated]


class UpdateOrderViewSet(mixins.ListModelMixin,
                         mixins.UpdateModelMixin,
                         mixins.RetrieveModelMixin,
                         viewsets.GenericViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    pagination_class = UserOrderPagination
    permission_classes = [IsAuthenticated]
