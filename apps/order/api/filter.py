import django_filters as filters
from apps.order.models import *
from django import forms


class OrderFilter(filters.FilterSet):
    assessment_type = filters.MultipleChoiceFilter(choices=Order.CategoryChoices.choices,
                                                   widget=forms.CheckboxSelectMultiple())

    status = filters.MultipleChoiceFilter(choices=Order.CategoryChoices.choices,
                                          widget=forms.CheckboxSelectMultiple())

    class Meta:
        model = Order
        fields = ['user']
