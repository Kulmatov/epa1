from django.urls import path
from rest_framework import routers

from apps.order.api.views import *
from apps.order.api.viewset import *

router = routers.DefaultRouter()

router.register(r'orders', OrderViewSet, basename='order')  # PROFILE
router.register(r'update/orders', UpdateOrderViewSet, basename='order')  # Update order in PROFILE

urlpatterns = router.urls

urlpatterns += [

    path('create/order/', order_creat),  # FORM1
    path('fill/aplications/', fill_aplication),  # MAIN
]
