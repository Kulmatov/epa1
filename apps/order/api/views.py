from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from twilio.rest import Client

from apps.order.api.serializers import OrderSerializer
from apps.order.models import *

from rest_framework import status
from django.core.files.storage import FileSystemStorage


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def order_creat(request):
    assessment_type = request.data["choose_the_direction_of_assessment"]
    status = request.data["status"]
    last_name = request.data["last_name"]
    first_name = request.data["first_name"]
    patronymic = request.data["patronymic"]
    email = request.data["email"]
    phone_number = request.data["phone_number"]
    place_residence = request.data["place_residence"]
    full_name_owner = request.data["full_name_owner"]
    owner_date_death = request.data["owner_date_death"]
    death_certificate = request.data["death_certificate"]
    vin = request.data["vin"]
    model_car = request.data["model_car"]
    year_issue = request.data["year_issue"]
    notarial_place = request.data["notarial_place"]
    # img = request.POST.get('img')
    img = request.FILES['img']
    fss = FileSystemStorage()
    file = fss.save(img.name, img)

    order = Order.objects.filter(vin=vin, assessment_type=assessment_type).first()
    if order:
        res = {
            "status": 0,
            "error": "error order already exists"
        }
        return Response(res)
    else:
        order = Order.objects.create(
            assessment_type=assessment_type,
            status=status,
            last_name=last_name,
            first_name=first_name,
            patronymic=patronymic,
            email=email,
            place_residence=place_residence,
            order_phone_number=phone_number,
            full_name_owner=full_name_owner,
            owner_date_death=owner_date_death,
            death_certificate=death_certificate,
            vin=vin,
            model_car=model_car,
            year_issue=year_issue,
            notarial_place=notarial_place,
            img=file,
            user=request.user
        )
        order.save()
        return Response({"status": 1, "msg": "order created!"})


def send_sms(admin_number, text):
    try:
        sid = 'AC8a8dd1623562a69bf0373424648fad9d'
        auth_token = '771641e17def9034702a92200fa63627'
        client = Client(sid, auth_token)

        message = client.messages.create(
            body=text,
            from_='+18316182249',
            to=admin_number
        )
        return 'sms code sended'
    except Exception as ex:
        return 'Something was wrong ..'


@api_view(["POST"])
@permission_classes([AllowAny])
def fill_aplication(request):
    name = request.data['name']
    phone_number = request.data['phone_number']
    aplication = CallApplication.objects.filter(phone_number=phone_number).first()
    if not aplication:
        aplication = CallApplication.objects.create(
            name=name,
            phone_number=phone_number
        )
        aplication.save()
        admin_number = '+998908230595'
        send_sms(admin_number, "Новая заявка\nимя " + name + '\n' + 'номер клиента ' + str(phone_number))
        res = {
            'status': 1,
            'msg': 'sms sended',
        }
        return Response(res, status=status.HTTP_200_OK)
    else:
        res = {
            "status": 0,
            "error": "error such\napplication already exists"
        }
    return Response(res)
