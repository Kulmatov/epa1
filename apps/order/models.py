from django.db import models
from datetime import date
from apps.account.models import User


# Create your models here.


class Order(models.Model):
    class CategoryChoices(models.TextChoices):
        to_receive_an_inheritance = '1'
        for_division_of_property = '2'
        other = '3'

    class StatusChoices(models.TextChoices):
        fulfilled = '1'
        new = '2'
        in_process = '3'
        mot_paid = '4'

    assessment_type = models.CharField(max_length=2, choices=CategoryChoices.choices)
    status = models.CharField(max_length=2, choices=CategoryChoices.choices)
    last_name = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    patronymic = models.CharField(max_length=50)
    email = models.EmailField(null=True, blank=True)
    order_phone_number = models.CharField(max_length=100, blank=True)
    place_residence = models.CharField(max_length=100)
    full_name_owner = models.CharField(max_length=255)
    owner_date_death = models.DateField(default=date.today, blank=True, null=True)
    death_certificate = models.CharField(max_length=50)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    vin = models.CharField(max_length=100)
    model_car = models.CharField(max_length=100)
    year_issue = models.DateField(default=date.today, blank=True, null=True)
    notarial_place = models.CharField(max_length=100)
    img = models.ImageField(upload_to="order/")

    def __str__(self):
        return self.user.first_name


class CallApplication(models.Model):
    name = models.CharField(max_length=50, blank=True)
    phone_number = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.name
