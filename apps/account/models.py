from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.


class User(AbstractUser):
    phone_number = models.CharField(max_length=100, unique=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    patronymic = models.CharField(max_length=50)
    place_residence = models.CharField(max_length=100)
    sms_code = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.username
