import random
from django.contrib import messages
from django.core.mail import send_mail
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

from apps.account.api.registrations_views import send_sms
from apps.account.api.serializers import *
from apps.account.models import User
from config import settings

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


# PASSWORD
@api_view(["POST"])
@permission_classes([AllowAny])
def number_confirmation(request):
    try:
        phone_number = request.data['phone_number']
        user = User.objects.filter(username=phone_number).first()
        if user:
            sms_code = random.randint(10000, 99999)
            user.sms_code = sms_code
            user.save()
            send_sms(phone_number, "Sizning tasdiqlash codingiz: " + str(sms_code))
            res = {
                "status": 1,
                "msg": "successful",
                "user": UserSerializer(user, many=False, context={"request": request}).data,
                # "token": token
            }
        else:
            res = {
                "status": 0,
                "msg": "EROR",
            }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }
    return Response(res)


# PERSONAL
@api_view(["POST"])
@permission_classes([AllowAny])
def personal_confirmation(request):
    try:
        sms_code = request.data['sms_code']
        # email = request.data['email']
        user = User.objects.filter(sms_code=sms_code).first()
        if user and str(user.sms_code) == str(sms_code):
            # payload = jwt_payload_handler(user)
            # token = jwt_encode_handler(payload)
            user.save()
            res = {
                "status": 1,
                "msg": "successful",
                "user": UserSerializer(user, many=False, context={"request": request}).data,
                # "token": token
            }
        else:
            res = {
                "status": 0,
                "msg": "EROR",
            }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }
    return Response(res)


# @api_view(["GET"])
# @permission_classes([AllowAny])
# def user_confirmation(request):
#     try:
#         phone_number = request.data['phone_number']
#         yes = request.data['yes']
#         user = User.objects.filter(username=phone_number).first()
#         if user and yes != 0:
#             payload = jwt_payload_handler(user)
#             token = jwt_encode_handler(payload)
#             res = {
#                 "status": 1,
#                 "msg": "successful",
#                 "user": UserSerializer(user, many=False, context={"request": request}).data,
#                 "token": token
#             }
#         else:
#             res = {
#                 "status": 0,
#                 "msg": "EROR",
#             }
#         return Response(res)
#     except KeyError:
#         res = {
#             "status": 0,
#             "error": "Key error"
#         }
#     return Response(res)


# NEW PASSWORD
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def new_password(request):
    try:
        new_password = request.data['new_password']
        confirm = request.data['confirm_password']
        user = request.user
        if user and new_password == confirm:
            # a_user = authenticate(username=user.username)
            # if a_user is not None:
            user.set_password(new_password)
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            user.save()
            res = {
                "status": 1,
                "msg": "password changed successful",
                "user": UserSerializer(user, many=False, context={"request": request}).data,
                "token": token
            }
        else:
            res = {
                "status": 0,
                "error": "err old password"
            }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }

    return Response(res)


@api_view(["POST"])
@permission_classes([AllowAny])
def email_send(request):
    try:
        sms_code = random.randint(10000, 99999)
        message = 'Пароль для восстановление\n' + str(sms_code)
        email = request.data['email']
        user = User.objects.filter(email=email).first()
        if user:
            subject = 'ЭПА'
            send_mail(subject, message, settings.EMAIL_HOST_USER, [email], fail_silently=False)
            messages.success(request, 'Success!')
            user.sms_code = sms_code
            user.save()
            res = {
                'msg': 'sended Success',
                "user": UserSerializer1(user, many=False, context={"request": request}).data,
            }
            return Response(res)
        else:
            res = {
                'msg': 'err'
            }
            return Response(res)
    except KeyError:
        res = {
            'status': 0,
            'msg': 'nod found '
        }
        return Response(res)


@api_view(["POST"])
@permission_classes([AllowAny])
def email_confirmation(request):
    try:
        sms_code = request.data['sms_code']
        email = request.data['email']
        user = User.objects.filter(email=email).first()
        if user and str(user.sms_code) == str(sms_code):
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            user.save()
            res = {
                "status": 1,
                "msg": "successful",
                "user": UserSerializer(user, many=False, context={"request": request}).data,
                "token": token
            }
        else:
            res = {
                "status": 0,
                "msg": "EROR",
            }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }
    return Response(res)
