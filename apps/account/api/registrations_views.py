from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from rest_framework import status
import random
from twilio.rest import Client
from rest_framework.decorators import api_view, permission_classes

from apps.account.api.serializers import *
from apps.account.models import User

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


def send_sms(phone_number, text):
    try:
        sid = 'AC8a8dd1623562a69bf0373424648fad9d'
        auth_token = '771641e17def9034702a92200fa63627'
        client = Client(sid, auth_token)

        message = client.messages.create(
            body=text,
            from_='+18316182249',
            to=phone_number
        )
        return 'sms code sended'
    except Exception as ex:
        return 'Something was wrong ..'


# REG1
@api_view(["POST"])
@permission_classes([AllowAny])
def registrations(request):
    try:
        phone_number = request.data["phone_number"]
        email = request.data["email"]
        password = request.data["password"]
        confirm_password = request.data["confirm_password"]
        if password == confirm_password:
            user = User.objects.filter(username=phone_number).first()
            if not user:
                user = User.objects.create(
                    username=phone_number,
                    phone_number=phone_number,
                    email=email,
                )
                user.set_password(password)
                user.save()
                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                res = {
                    "status": 1,
                    "msg": "successful",
                    # "user": UserSerializer(user, many=False, context={"request": request}).data,
                    "token": token
                }
                return Response(res, status=status.HTTP_200_OK)
            else:
                res = {
                    'status': 0,
                    'msg': 'such an account already exists'
                }
                return Response(res)
        else:
            res = {
                'status': 0,
                'msg': 'password did not match'
            }
            return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }
        return Response(res)


# REG2
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def registrations2(request):
    last_name = request.data.get('last_name')
    first_name = request.data.get('first_name')
    patronymic = request.data.get('patronymic')
    place_residence = request.data.get('place_residence')
    user = request.user
    user_model = User.objects.filter(pk=user.id).first()
    if user_model:
        user_model.username = first_name
        user_model.last_name = last_name
        user_model.first_name = first_name
        user_model.patronymic = patronymic
        user_model.place_residence = place_residence
        user_model.save()
        sms_code = random.randint(10000, 99999)
        user_model.sms_code = sms_code
        phone_number = user_model.phone_number
        send_sms(phone_number, "Sizning tasdiqlash codingiz: " + str(sms_code))
        payload = jwt_payload_handler(user_model)
        token = jwt_encode_handler(payload)
        user_model.save()
        res = {
            "status": 1,
            "error": "created successfully and sended message",
            "token": token
        }
    else:
        res = {
            "status": 0,
            "error": "error"
        }
    return Response(res)


# CODE
@api_view(["POST"])
@permission_classes([AllowAny])
def accept_code(request):
    try:
        sms_code = request.data['sms_code']
        phone_number = request.data['phone_number']
        user = User.objects.filter(phone_number=phone_number).first()
        if user and str(user.sms_code) == str(sms_code):
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            user.save()
            res = {
                "status": 1,
                "msg": "successful",
                "user": UserSerializer(user, many=False, context={"request": request}).data,
                "token": token
            }
        else:
            res = {
                "status": 0,
                "msg": "EROR",
            }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }
    return Response(res)
