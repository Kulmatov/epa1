from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
import random
from django.contrib.auth import authenticate
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

from apps.account.api.registrations_views import send_sms
from apps.account.api.serializers import UpdateProfileSerializer, UserSerializer
from apps.account.models import User

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class SettingProfileViewSet(mixins.ListModelMixin,
                            mixins.RetrieveModelMixin,
                            viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [AllowAny]


class UpdateProfileView(RetrieveUpdateAPIView):
    permission_classes([IsAuthenticated])
    queryset = User.objects.all()
    serializer_class = UpdateProfileSerializer
    lookup_field = 'pk'


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def update_number(request):
    try:
        phone_number = request.data["new_phone_number"]
        user = request.user
        user2 = User.objects.filter(phone_number=phone_number).first()
        if not user2:
            user.phone_number = phone_number
            user.username = phone_number
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            sms_code = random.randint(10000, 99999)
            send_sms(phone_number, "Sizning tasdiqlash codingiz: " + str(sms_code))
            user.sms_code = sms_code
            user.save()
            res = {
                'status': 1,
                'msg': 'sms sended',
                "user": UserSerializer(user, many=False, context={"request": request}).data,
                'token': token
            }
            return Response(res, status=status.HTTP_200_OK)
        else:
            res = {
                'status': 0,
                'msg': 'try entering the number again'
            }
            return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }
        return Response(res)


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def message_confirmation(request):
    try:
        sms_code = request.data['sms_code']
        phone_number = request.data['phone_number']
        user = User.objects.filter(phone_number=phone_number).first()
        if user and str(sms_code) == str(user.sms_code):
            user.sms_code = sms_code
            user.save()
            res = {
                "status": 1,
                "msg": "successful",
                "user": UserSerializer(user, many=False, context={"request": request}).data
            }
        else:
            res = {
                "status": 0,
                "msg": "user not found",
            }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }
        return Response(res)


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def update_password(request):
    try:
        old_password = request.data['old_password']
        new_password = request.data['new_password']
        confirm = request.data['confirm_password']
        user = request.user
        if user and new_password == confirm:
            a_user = authenticate(username=user.username, password=old_password)
            if a_user is not None:
                a_user.set_password(new_password)
                a_user.save()
                res = {
                    "password changed successfully"
                }
            else:
                res = {
                    "status": 0,
                    "error": "err old password"
                }
        else:
            res = {
                "status": 0,
                "error": "wrong password"
            }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }

    return Response(res)
