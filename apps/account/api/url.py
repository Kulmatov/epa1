from django.urls import path
from rest_framework import routers

# from rest_framework.routers import DefaultRouter
from apps.account.api.login_views import login
from apps.account.api.registrations_views import *
from apps.account.api.reset_password import *
from apps.account.api.update_views import *

router = routers.DefaultRouter()
router.register(r'setting', SettingProfileViewSet, basename='user')  # SETTING

urlpatterns = router.urls
urlpatterns += [

    path('reg1/', registrations),  # REG1
    path('reg2/', registrations2),  # REG2

    path('code/', accept_code),  # CODE

    path('login/', login),  # LOGIN

    path('recovery/<int:pk>/', UpdateProfileView.as_view()),  # SETTING SAVE
    path('update/number/', update_number),  # SETTING SAVE
    path('sms/number/', message_confirmation),  # SETTING SAVE
    path('update/password/', update_password),  # SETTING SAVE

    path('number/confirmation/', number_confirmation),  # PASSWORD
    path('user/confirmation/', personal_confirmation),  # PASSWORD
    # path('user/confirmation/', user_confirmation),  # PASSWORD
    path('new/password/', new_password),  # NEW PASSWORD

    path('email/', email_send),  # EMAIL
    path('email/confirmation/', email_confirmation),  # EMAIL

]
