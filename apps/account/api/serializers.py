from rest_framework import serializers
from apps.account.models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"


class UpdateProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['last_name', 'first_name', 'patronymic', 'place_residence', 'email']


class UserSerializer1(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email']
