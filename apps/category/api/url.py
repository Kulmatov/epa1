from rest_framework import routers

# from rest_framework.routers import DefaultRouter
from apps.category.api.viewset import *

router = routers.DefaultRouter()

router.register(r'category', TypeAssessmentsViewSet, basename='typeassessments')
router.register(r'products', ProductViewSet, basename='product')

urlpatterns = router.urls
