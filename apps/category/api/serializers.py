from rest_framework import serializers
from apps.category.models import *


class TypeAssessmentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeAssessments
        fields = "__all__"


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"