from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny
from apps.category.api.filter import *
from apps.category.api.serializers import *
from apps.category.models import Product


class TypeAssessmentsViewSet(mixins.ListModelMixin,
                             mixins.RetrieveModelMixin,
                             viewsets.GenericViewSet):
    queryset = TypeAssessments.objects.all()
    serializer_class = TypeAssessmentsSerializer
    permission_classes = [AllowAny]


class ProductViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filterset_class = ProductFilter
    permission_classes = [AllowAny]
