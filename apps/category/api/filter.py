import django_filters as filters
from apps.category.models import *


class ProductFilter(filters.FilterSet):
    class Meta:
        model = Product
        fields = ['type_assessments']
