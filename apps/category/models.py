from django.db import models


# Create your models here.


class TypeAssessments(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Product(models.Model):
    product_name = models.CharField(max_length=100)
    desc = models.CharField(max_length=255)
    img = models.ImageField(default='')
    price = models.FloatField(default=0)
    type_assessments = models.ForeignKey(TypeAssessments, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.product_name
