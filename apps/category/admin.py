from django.contrib import admin

# Register your models here.
from apps.category.models import *

admin.site.register(TypeAssessments)
admin.site.register(Product)